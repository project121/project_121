Rails.application.routes.draw do
  devise_for :admins

  devise_scope :admin do
    authenticated :admin do
      root 'users#index', as: :authenticated_root
      get '/admins/sign_out' => 'devise/sessions#destroy'
    end

    unauthenticated do
        root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :users
  resources :minimum_transactions
  resources :rewards

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :users, only: [:index] do
        collection do
          post :signup
          post :login
          get :logout
          get :user_details
          post :update_user
          post :confirm_user
          post :resend_otp
          post :password_forget
          post :password_reset
          post :update_password
          post :social_login
          post :rewards_claim
          get :rewards_history
        end
      end
      resources :coins do
        collection do
          post :all_coin_details
          post :coin_details
        end
      end
      resources :rewards do
        collection do
          post :all_rewards
        end
      end
      resources :leaderboards, only:[:index] do
      end
      resources :user_transactions do
        collection do
          post :coin_transaction
          get :transaction_detail
          post :coin_transaction_detail
        end
      end
    end
  end
end
