class UserMailer < ApplicationMailer

  def send_otp(user)
    @user = user
    mail(to: @user.email, subject: 'Account Verification')
  end

  def reset_password(user)
    @user = user
    mail(to: @user.email, subject: "Password Reset")
  end
end
