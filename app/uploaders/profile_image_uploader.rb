# encoding: utf-8

class ProfileImageUploader < CarrierWave::Uploader::Base

  include Cloudinary::CarrierWave

  version :thumb do
    process :scale => [50, 50]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
