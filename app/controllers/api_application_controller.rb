class ApiApplicationController < ActionController::API

  def unauthorized_json
  {
    status: "false",
    message: "Access denied"
  }
  end
 
  private

  def check_if_device_present
    unless device_present
      render json: unauthorized_json, status: :unauthorized, code: 401
    end
  end

  def device_present
    @@device_id = request.headers['DeviceId']
    @@device_type = request.headers['DeviceType']
    return true if @@device_id && @@device_type
  end

  def check_if_current_api_user
    unless current_api_user
      render json: {status: false, message: 'Auth not available or already logout.' }, status: 401
    end
  end

  def current_api_user
    return @current_user if @current_user
    if auth_token = request.headers['Authorization']
      @current_user = User.where(auth_token: auth_token).first
    end
  end

  def render_not_found
    render json: { error: 'Record not found' }, status: 404, head: :not_found
  end

  def render_ok
    render json: { message: 'ok' }
  end
end
