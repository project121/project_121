class Api::V1::CoinsController < ApiApplicationController
  before_action :check_if_current_api_user
  before_action :set_page_params

  def all_coin_details
    @coin = Coin.page(@page).per(@per_page)
    @details = []
    @coin.each do |coin|
      detail = {}
      detail[:coin_name] = coin.name
      detail[:coin_code] = coin.code
      detail[:coin_image] = coin.image_url
      @coin_detail = Cryptocompare::Price.full(coin.code, 'USD')
      @coin_detail_last_hour = Cryptocompare::HistoHour.find(coin.code, 'USD')['Data'].last
      detail[:change_in_1_hour] = @coin_detail_last_hour['close'].to_s
      detail[:change_in_24_hour] = @coin_detail["DISPLAY"][coin.code]["USD"]["CHANGE24HOUR"]
      detail[:VOLUME24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["VOLUME24HOUR"]
      detail[:OPEN24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["OPEN24HOUR"]
      detail[:HIGH24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["HIGH24HOUR"]
      detail[:LOW24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["LOW24HOUR"]
      detail[:CHANGE24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["CHANGE24HOUR"]
      detail[:coin_symbol] = @coin_detail["DISPLAY"][coin.code]["USD"]["FROMSYMBOL"]
      detail[:dollar_symbol] = @coin_detail["DISPLAY"][coin.code]["USD"]["TOSYMBOL"]
      detail[:coin_price]  = @coin_detail["RAW"][coin.code]["USD"]["PRICE"]
      detail[:coin_supply] = @coin_detail["DISPLAY"][coin.code]["USD"]["SUPPLY"]
      detail[:coin_market_cap] = @coin_detail["DISPLAY"][coin.code]["USD"]["MKTCAP"]
      @details << detail
    end
    wallet_amount = MasterWallet.where(user_id: @current_user.id).last
    render json: { status: 'true', data: @details, user_wallet_amount: wallet_amount.amount.to_s },  status: 200
  end

  def coin_details
    coin = Coin.where(code: params[:coin_code]).first
    if coin.present?
      @details = []
      detail = {}
      detail[:coin_image] = coin.image_url
      @coin_detail = Cryptocompare::Price.full(coin.code, 'USD')
      @coin_detail_last_hour = Cryptocompare::HistoHour.find(coin.code, 'USD')['Data'].last
      detail[:coin_name] = coin.name
      detail[:coin_code] = coin.code
      detail[:coin_symbol] = @coin_detail["DISPLAY"][coin.code]["USD"]["FROMSYMBOL"]
      detail[:dollar_symbol] = @coin_detail["DISPLAY"][coin.code]["USD"]["TOSYMBOL"]
      detail[:change_in_1_hour] = @coin_detail_last_hour['close'].to_s
      detail[:change_in_24_hour] = @coin_detail["DISPLAY"][coin.code]["USD"]["CHANGE24HOUR"]
      detail[:VOLUME24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["VOLUME24HOUR"]
      detail[:OPEN24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["OPEN24HOUR"]
      detail[:HIGH24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["HIGH24HOUR"]
      detail[:LOW24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["LOW24HOUR"]
      detail[:CHANGE24HOUR] = @coin_detail["DISPLAY"][coin.code]["USD"]["CHANGE24HOUR"]
      detail[:coin_price] = @coin_detail["DISPLAY"][coin.code]["USD"]["PRICE"]
      detail[:coin_supply] = @coin_detail["DISPLAY"][coin.code]["USD"]["SUPPLY"]
      detail[:coin_market_cap] = @coin_detail["DISPLAY"][coin.code]["USD"]["MKTCAP"]
      @details << detail
      wallet_amount = MasterWallet.where(user_id: @current_user.id).last
      render json: { status: 'true', data: @details, user_wallet_amount: wallet_amount.amount.to_s },  status: 200
    else
      render json: { status: 'false', message: 'Coin not in database.'},  status: 404
    end
  end

  private

  def set_page_params
    @page = params[:page] || 1
    @per_page = params[:per_page] || 10
  end
end
