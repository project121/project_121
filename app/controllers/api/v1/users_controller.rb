class Api::V1::UsersController < ApiApplicationController
  before_action :check_if_current_api_user, except: [:signup, :login, :confirm_user, :resend_otp, :password_forget, :password_reset, :social_login]
  before_action :check_if_device_present, only:[:signup, :login, :confirm_user, :resend_otp, :password_forget, :password_reset]

  def signup
    @user = User.new(user_params)
    if email_present?(user_params)
      render json: {status: false, message: 'Email already exists.' }, status: 422
    elsif phone_present?(user_params)
      render json: {status: false, message: 'Phone already exists.' }, status: 422
    else
      if @user.save
        @user.update_attributes!(auth_token: User.generate_token)
        wallet = @user.create_master_wallet(amount: 10000.00)
        LeaderBoard.create(user_id: @user.id, net_worth: wallet.amount,
                                               growth_rate: 0.to_f)
        add_user_device
        send_verification_mail(@user)
        response.header['otp'] = @user.otp
        verify_account(200)
      else
        render json: {status: false, message:  @user.errors }, status: 422
      end
    end
  end

  def add_user_device
    @user.try(:device).destroy if @user.device
    @user.create_device(device_type: request.headers['DeviceId'], device_id: request.headers['DeviceType'])
  end

  def resend_otp
    @user = User.find_by_email(params.require(:email))
    if @user.present?
      if user_active?(@user)
        render json: {status: false, message: 'User Already active' }, status: 401
      else
        send_verification_mail(@user)
        response.header['otp'] = @user.otp
        verify_account(200)
      end
    else
      render json: {status: false, message: 'Please enter correct Email.' }, status: 401
    end
  end

  def confirm_user
    @user = User.where(otp: params[:otp]).first
    if @user.present?
      @user.update_columns(:otp_confirmed_at => Time.now, :active => "true", :otp => nil)
      response.header['auth_token'] = @user.auth_token
      user_info(@user, "The verification has been successful!")
    else
      render json: {status: false, message: 'Incorrect OTP.' }, status: 401
    end
  end

  def login
    @user = User.find_by_email(params.require(:email))
    if @user.present?
      if user_active?(@user)
        if @user &&  @user.authenticate(params[:password])
          @user.update_attribute(:auth_token, User.generate_token)
          add_user_device
          response.header['auth_token'] = @user.auth_token
          user_info(@user, "The sign in has been successful!")
        else
          render json: {status: false, message: "Invalid email or password."}, status: 401
        end
      else
        verify_account(202)
      end
    else
      render json: {status: false, message: "Invalid email or password."}, status: 401
    end
  end

  def logout
    if @current_user.present?
      @current_user.update_attribute(:auth_token, nil)
      render json: { status: 'true', message: 'Logout successfully' }
    else
      render json: { status: 'false', message: 'User Not found.' }
    end
  end

  def user_details
    @user = @current_user if @current_user
    user_info(@user, "null")
  end

  def update_user
    @user = @current_user if @current_user.update_columns(name: params[:name])
    user_info(@user, "null")
  end

  def update_password
    if @current_user.authenticate(params[:password]).present?
      if params[:new_password] == params[:confirm_password]
        @current_user.update_attribute(:password, params[:new_password])
        render json: {status: true, message: "Password updated successfully!"}, status: 200
      else
        render json: {status: false, message: "password confirm is not match with new password"}, status: 400
      end
    else
      render json: {status: false, message: "Please enter correct password"}, status: 400
    end
  end

  def send_otp(user)
    otp = SecureRandom.hex(3)
    otp_sent_at = Time.now.utc
    client = Twilio::REST::Client.new('ACe828fe2e7fa0c36029c85b0d8b040230', '2ad987391b9bc08402b93493b8be8b7e')
    message = client.messages.create :to => "+91"+"#{user.phone}", :from => '+15202145420', :body => "Please activate your account by using otp:"+ " " + "#{otp}"
    user.update!(otp: otp, otp_sent_at: otp_sent_at)
  end

  def send_verification_mail(user)
    otp = rand.to_s[2..5]
    otp_sent_at = Time.now.utc
    user.update_columns(otp: otp, otp_sent_at: otp_sent_at, otp_confirmed_at: nil, active: 'false')
    #UserMailer.send_otp(@user).deliver
  end

  def password_forget
    @user = User.find_by_email(params.require(:email))
    if @user.present?
      @user.create_reset_digest
      UserMailer.reset_password(@user).deliver_now
      render json: {status: true, message: "Email sent with reset code successfully!"}, status: 200
    else
      render json: {status: false, message: 'User not found!'}, status: 404
    end
  end

  def password_reset
    reset = User.find_by(reset_digest: params[:reset_digest])
    if reset.present?
      reset.update_attributes!(reset_params)
      reset.update_attribute(:reset_digest,nil)
      render json: {status: true, message: "Password updated successfully!"}, status: 200
    else
       render json: {status: false, message: "Please enter correct temporary password"}, status: 400
    end
  end

  def social_login
    @user = User.find_by(social_id: params[:social_id])
    if @user.present?
      if user_active?(@user)
        @user.update_attribute(:social_access_token,params[:social_access_token])
        @user.update_attribute(:auth_token, User.generate_token)
        add_user_device
        response.header['auth_token'] = @user.auth_token
        user_info(@user, "The sign in has been successful!")
      else
        verify_account(202)
      end
    else
        render json: { status: false, message: "User not found with social_login" }, status: 404
    end
  end

  def rewards_history
    @reward_list = UserReward.where("user_id = ?",@current_user.id)
    if @reward_list.present?
      @list = []
      @reward_list.each do |list|
        details = {}
        details[:id] = list.reward.id
        details[:company_name] = list.reward.company_name
        details[:company_logo] = list.reward.company_logo
        details[:price] = list.reward.price
        details[:expiry] = list.reward.expiry
        details[:offer] = list.reward.offer
        details[:description] = list.reward.description
        details[:reward_code] = list.reward.reward_code
        @list << details
      end
      wallet_amount = MasterWallet.where(user_id: @current_user.id).last
      render json: { status: 'true', message: 'Success', data: @list, user_wallet_amount: wallet_amount.amount.to_s }, status: 200
    else
      render json: { status: 'false', message: 'No rewards history.'}, status: 404
    end
  end

  def rewards_claim
    @reward = Reward.find_by_id(params[:reward_id])
    if @reward.present?
      @user_reward = UserReward.where("user_id=? AND reward_id = ?", @current_user.id, @reward.id)
      if @user_reward.present?
        render json: { status: true, message: "You already claimed this reward" }, status: 402
      else
        @user_amount = @current_user.try(:master_wallet).amount
        if @user_amount > @reward.price.to_f
          UserReward.create!(user_id: @current_user.id, reward_id: @reward.id)
          @new_user_amount = @user_amount - @reward.price.to_f
          @current_user.master_wallet.update_attribute(:amount,@new_user_amount)
          @current_user.user_transaction.create(user_id: @current_user.id, amount: @reward.price, transaction_type: "reward claim")
          wallet_amount = MasterWallet.where(user_id: @current_user.id).last
          render json: { status: true, message: "Reward Claimed", data: {reward_details: {id: @reward.id,comapny_name: @reward.company_name, comapny_logo: @reward.company_logo, price: @reward.price, offer: @reward.offer, expiry: @reward.expiry, description: @reward.description, reward_code: @reward.reward_code,wallet_amount: wallet_amount.amount.to_s,}}}, status: 200
        else
          render json: { status: false, message: "Not have enough money to claim reward" }, status: 400
        end
      end
    end
  end

  private

  def user_params
    params.permit(:name, :email, :password, :password_confirmation, :phone, :otp, :profile_image, :social_access_token, :social_id, :social_type)
  end

  def reset_params
    params.permit(:password, :password_confirmation)
  end

  def verify_account(status)
    render json: { status: true, message: "We have sent a mail at your registered email along with an OTP. Please verify your OTP." }, status: "#{status}"
  end

  def email_present?(user_params)
    User.where(email: params[:email]).present?
  end

  def phone_present?(user_params)
    User.where(phone: params[:phone]).present?
  end

  def not_found
    render json: { message: 'User not found.' }
  end

  def user_active?(user)
    user.active == 'true'
  end

  def user_info(user, message)
    wallet_amount = MasterWallet.where(user_id: user.id).last
    render json: { status: 'true', message: "#{message}" , data: {user_details: {id: user.id, first_name: user.name, email: user.email, mobile_no: user.phone,profile_image: user.profile_image, created_at: user.created_at, wallet_amount: wallet_amount.amount.to_s } } }
  end
end
