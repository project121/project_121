class Api::V1::UserTransactionsController < ApiApplicationController
  before_action :check_if_current_api_user

  def coin_transaction
    @amount = params[:amount].to_f
    @master_wallet_amount = @current_user.master_wallet.amount
    @minimum_transaction = MinimumTransaction.last
    if params[:transaction_type] == 'Buy'
      if @master_wallet_amount < @amount + @minimum_transaction.amount
        render json: { status: 'false', message: 'Your wallet amount is low.'},  status: 404
      else
        user_coin = @current_user.user_coins.where('coin_name = ?', params[:coin_name]).last
        total_user_coin = user_coin.try(:total_coin_count)
        if total_user_coin.present?
          total_user_coin
        else
          total_user_coin = 0
        end
        new_total_coin_count = total_user_coin.to_d + params[:coin_count].to_d
        @buy_transaction = @current_user.user_transaction.create(amount: params[:amount], transaction_type: params[:transaction_type])
        coin_base = @buy_transaction.user_coin.create(coin_name: params[:coin_name], coin_count: params[:coin_count], coin_price_at_time: params[:coin_price_at_time], user_id: @current_user.id, total_coin_count: new_total_coin_count)
        coin_base.save
        new_amount = @master_wallet_amount - @amount
        master_wallet_update = @current_user.master_wallet.update_columns(amount: new_amount)
        render json: { status: 'true', message: 'Buying coin successful' }, status: 200
      end
    elsif params[:transaction_type] == 'Sell'
      user_coin = @current_user.user_coins.where('coin_name = ?', params[:coin_name]).last
      total_user_coin = user_coin.try(:total_coin_count)
      if total_user_coin.present?
        total_user_coin
      else
        total_user_coin = 0
      end
      new_total_coin_count = total_user_coin.to_d - params[:coin_count].to_d
      @sell_transaction = @current_user.user_transaction.build(user_id: @current_user.id, amount: params[:amount], transaction_type: params[:transaction_type])
      coin_base = @sell_transaction.user_coin.build(coin_name: params[:coin_name], coin_count: params[:coin_count], coin_price_at_time: params[:coin_price_at_time], user_id: @current_user.id, total_coin_count: new_total_coin_count)
      coin_base.save
      new_amount = @master_wallet_amount + @amount
      master_wallet_update = @current_user.master_wallet.update_columns(amount: new_amount)
      render json: { status: 'true', message: 'Selling coin successful' }, status: 200
    end
  end

  def transaction_detail
    @details = UserCoin.where("user_id = ?", @current_user.id)
    if @details.present?
      @data = []
      @details.each do |details|
        coins = {}
        coins[:coin_name] = details.coin_name
        @data << coins
      end
       @coin = @data.uniq
       @coin_detail = []
       @coin.each do |coin_detail|
        coin_data = {}
        coin = Coin.where(code: coin_detail[:coin_name]).first
        coin_data[:coin_name] = coin.name
        coin_data[:coin_code] = coin.code
        coin_data[:image_url] = coin.image_url
        @coin_api_detail = Cryptocompare::Price.full(coin.code, 'USD')
        coin_data[:coin_price] = @coin_api_detail["RAW"][coin.code]["USD"]["PRICE"]
        @coin_detail_last_hour = Cryptocompare::HistoHour.find(coin.code, 'USD')['Data'].last
        coin_data[:change_in_1_hour] = @coin_detail_last_hour['close'].to_s
        user_coin = @current_user.user_coins.where('coin_name = ?', coin.code).last
        coin_data[:coin_count] = user_coin.total_coin_count
        @coin_detail << coin_data
       end
       wallet_amount = MasterWallet.where(user_id: @current_user.id).last
       render json: { status: 'true', message: 'Success', data: @coin_detail, user_wallet_amount: wallet_amount.amount.to_s }, status: 200
    else
      render json: { status: 'false', message: 'No detail found'}, status: 404
    end
  end

  def coin_transaction_detail
    @details = UserCoin.where("user_id = ? AND coin_name = ?",@current_user.id, params[:coin_name])
    if @details.present?
      @data = []
      @details.each do |details|
        detail = {}
        detail[:user_id] = details.user_id
        detail[:coin_name] = details.coin_name
        detail[:coin_count] = details.coin_count
        detail[:coin_price_at_time] = details.coin_price_at_time
        detail[:amount] = details.try(:user_transaction).amount
        detail[:transaction_type] = details.try(:user_transaction).transaction_type
        transaction_date = details.try(:user_transaction).created_at
        date = transaction_date.strftime( "%d/%m/%Y")
        detail[:transaction_date] = date
        @data << detail
      end
      wallet_amount = MasterWallet.where(user_id: @current_user.id).last
      render json: { status: 'true', message: 'Success', data: @data, user_wallet_amount: wallet_amount.amount.to_s }, status: 200
    else
      render json: { status: 'false', message: 'No detail found'}, status: 404
    end
  end
end
