class Api::V1::LeaderboardsController < ApiApplicationController
  before_action :check_if_current_api_user

  def index
    sql = "SELECT lb.*, u.name, rank() over (order by net_worth desc)
          FROM public.leader_boards lb join public.users u on lb.user_id = u.id
          ORDER BY rank ASC LIMIT 10"
    @net_worth = LeaderBoard.find_by_sql(sql)
    user_rank = "SELECT * FROM (SELECT *, rank() over (order by net_worth desc)
                FROM public.leader_boards ORDER BY rank ASC LIMIT 10)A WHERE user_id= #{@current_user.id}"
    rank = LeaderBoard.find_by_sql(user_rank)
    wallet_amount = MasterWallet.where(user_id: @current_user.id).last
    render json: { status: true, message: 'Leader Board', data: [@net_worth, user_info: rank], user_wallet_amount: wallet_amount.amount.to_s}, status: 200
  end
end
