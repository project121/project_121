class Api::V1::RewardsController < ApiApplicationController

  before_action :check_if_current_api_user

  def all_rewards
    @current_date = params[:current_date]
    @reward_list = @current_user.user_rewards.pluck("reward_id")
    if @reward_list.present?
      @rewards = Reward.where("expiry >= ?", @current_date).where("id NOT IN (?)",@reward_list).order("expiry")
    else
      @rewards = Reward.where("expiry >= ?", @current_date).order("expiry")
    end
    if @rewards.present?
      render json: { status: true, data: @rewards, message: 'Rewards history' }, status: 200
    else
      render json: { status: false, message: "No rewards available." }, status: 404
    end
  end
end
