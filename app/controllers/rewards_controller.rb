class RewardsController < ApplicationController

  def index
    @rewards = Reward.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @reward = Reward.new
  end

  def create
    @reward = Reward.new(reward_params.merge(reward_status: 'true'))
    if @reward.save
      redirect_to rewards_url, notice: 'Reward was successfully created.'
    else
      render :new
    end
  end

  def edit
    @reward = Reward.find(params[:id])
  end

  def update
    @reward = Reward.find(params[:id])
    if @reward.update(reward_params)
      redirect_to rewards_url, notice: 'Reward was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @reward = Reward.find(params[:id])
    @reward.destroy
    redirect_to rewards_url, notice: 'Reward destroyed successfully.'
  end

  private

  def reward_params
    params.require(:reward).permit(:company_name, :company_logo, :reward_image_url, :offer, :expiry, :description, :reward_code, :reward_status, :price)
  end
end
