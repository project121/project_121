class MinimumTransactionsController < ApplicationController

  def index
      @minimum = MinimumTransaction.all.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @minimum_amount = MinimumTransaction.new
  end

  def create
    @minimum_amount = MinimumTransaction.new(params.require(:minimum_transactions).permit(:amount))
    if @minimum_amount.save
      redirect_to minimum_transactions_path, notice: 'Amount was successfully created.'
    else
      render :new
    end
  end

end
