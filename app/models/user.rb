class User < ApplicationRecord

  has_one :device, dependent: :destroy
  has_one :master_wallet, dependent: :destroy
  has_many :user_transaction, dependent: :destroy
  has_many :user_coins, dependent: :destroy
  has_many :rewards, dependent: :destroy
  has_many :user_rewards, dependent: :destroy

  mount_uploader :profile_image, ProfileImageUploader

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name, presence: true, length: {maximum: 75}
  validates :phone, presence: true, numericality: {only_integer: true}, length: {maximum: 10, minimum: 10}, uniqueness: true
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }

  def self.generate_token
    SecureRandom.urlsafe_base64
  end

  def create_reset_digest
    reset_token = SecureRandom.hex(3)
    update_attribute(:reset_digest, reset_token)
    update_attribute(:reset_sent_at, Time.zone.now)
  end
end
