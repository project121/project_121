class UserTransaction < ApplicationRecord
  belongs_to :user
  has_many :user_coin

  validates :amount, presence: true
  validates :transaction_type, presence: true
end
