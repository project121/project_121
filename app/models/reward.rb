class Reward < ApplicationRecord
  has_many :rewards
  mount_uploader :company_logo, BrandLogoUploader
end
