class LeaderBoard < ApplicationRecord
  belongs_to :user

  def user_worth
    @users = User.where(active: 'true').pluck(:id)
    @users.each do |user|
      coins_worth = coin_amount_calculation(user).to_f
      rewards_worth = rewards_worth_calculation(user).to_f
      master_wallet_worth = master_wallet(user)
      net_worth = rewards_worth + coins_worth + master_wallet_worth
      growth_rate = delta_growth(user).to_f
      which_user = LeaderBoard.where(user_id: user).last
      worth = which_user.update_attributes!(user_id: user, net_worth: net_worth,
                                             growth_rate: growth_rate)
    end
  end

  def coin_amount_calculation(user)
    coin_amount = 0
    coins = UserCoin.where(user_id: user)
    coins.each do |coin|
      coin_name = coin.coin_name
      coin_data = Cryptocompare::Price.find('USD', coin_name)
      coin_value = coin_data['USD'][coin_name]
      coin_amount = coin_amount + coin_value
    end
    return coin_amount
  end

  def rewards_worth_calculation(user)
    reward_worth = 0
    rewards = UserReward.where(user_id: user)
    rewards.each do |reward|
      reward_value = Reward.find_by_id(reward.reward_id).price.to_i
      reward_worth = reward_worth + reward_value
    end
    return reward_worth
  end

  def master_wallet(user)
    master_wallet = MasterWallet.where(user_id: user).last
    master_wallet_worth = 0.to_f
    if master_wallet.present?
      master_wallet_worth = master_wallet.try(:amount)
    end
    return master_wallet_worth
  end

  def delta_growth(user)
    previous_worth = LeaderBoard.where(user_id: user).last.net_worth.to_f
    current_worth = rewards_worth_calculation(user).to_f
    delta_growth = (((previous_worth - current_worth)/previous_worth).to_f)*100
    return delta_growth
  end
end
