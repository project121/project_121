class CreateDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :devices do |t|
      t.references :user
      t.string :device_id
      t.string :device_type
      t.string :device_token
      t.timestamps
    end
  end
end
