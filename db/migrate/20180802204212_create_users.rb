class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :auth_token
      t.string :password_digest
      t.string :otp
      t.datetime :otp_sent_at
      t.datetime :otp_confirmed_at
      t.string :active
      t.string :reset_digest
      t.datetime :reset_sent_at

      t.timestamps
    end
  end
end
