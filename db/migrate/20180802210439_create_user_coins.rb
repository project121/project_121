class CreateUserCoins < ActiveRecord::Migration[5.0]
  def change
    create_table :user_coins do |t|
      t.string :coin_name
      t.string :coin_count
      t.string :coin_price_at_time
      t.references :user_transaction, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
