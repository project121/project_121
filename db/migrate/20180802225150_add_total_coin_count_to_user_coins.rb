class AddTotalCoinCountToUserCoins < ActiveRecord::Migration[5.0]
  def change
    add_column :user_coins, :total_coin_count, :string
  end
end
