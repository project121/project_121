class CreateRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :rewards do |t|
      t.string :company_name
      t.string :company_logo
      t.string :price
      t.string :offer
      t.date :expiry
      t.text :description
      t.string :reward_code
      t.string :reward_status
      
      t.timestamps
    end
  end
end
