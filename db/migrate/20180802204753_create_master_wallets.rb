class CreateMasterWallets < ActiveRecord::Migration[5.0]
  def change
    create_table :master_wallets do |t|
      t.references :user, foreign_key: true
      t.float :amount, default: 0.0
      t.timestamps
    end
  end
end
