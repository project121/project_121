class CreateLeaderBoards < ActiveRecord::Migration[5.0]
  def change
    create_table :leader_boards do |t|
      t.references :user, foreign_key: true
      t.float :net_worth

      t.timestamps
    end
  end
end
