class AddSocialLoginCredsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :social_id, :string
    add_column :users, :social_access_token, :string
  end
end
