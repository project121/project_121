class CreateUserTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_transactions do |t|
      t.references :user, foreign_key: true
      t.float :amount
      t.string :transaction_type
      t.timestamps
    end
  end
end
