class AddGrowthRateToLeaderBoards < ActiveRecord::Migration[5.0]
  def change
    add_column :leader_boards, :growth_rate, :float
  end
end
