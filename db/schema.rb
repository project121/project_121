# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181002162939) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "coins", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.text     "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "devices", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "device_id"
    t.string   "device_type"
    t.string   "device_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["user_id"], name: "index_devices_on_user_id", using: :btree
  end

  create_table "leader_boards", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "net_worth"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "growth_rate"
    t.index ["user_id"], name: "index_leader_boards_on_user_id", using: :btree
  end

  create_table "master_wallets", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "amount",     default: 0.0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["user_id"], name: "index_master_wallets_on_user_id", using: :btree
  end

  create_table "minimum_transactions", force: :cascade do |t|
    t.float    "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rewards", force: :cascade do |t|
    t.string   "company_name"
    t.string   "company_logo"
    t.string   "price"
    t.string   "offer"
    t.date     "expiry"
    t.text     "description"
    t.string   "reward_code"
    t.string   "reward_status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "user_coins", force: :cascade do |t|
    t.string   "coin_name"
    t.string   "coin_count"
    t.string   "coin_price_at_time"
    t.integer  "user_transaction_id"
    t.integer  "user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "total_coin_count"
    t.index ["user_id"], name: "index_user_coins_on_user_id", using: :btree
    t.index ["user_transaction_id"], name: "index_user_coins_on_user_transaction_id", using: :btree
  end

  create_table "user_rewards", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "reward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reward_id"], name: "index_user_rewards_on_reward_id", using: :btree
    t.index ["user_id"], name: "index_user_rewards_on_user_id", using: :btree
  end

  create_table "user_transactions", force: :cascade do |t|
    t.integer  "user_id"
    t.float    "amount"
    t.string   "transaction_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["user_id"], name: "index_user_transactions_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "auth_token"
    t.string   "password_digest"
    t.string   "otp"
    t.datetime "otp_sent_at"
    t.datetime "otp_confirmed_at"
    t.string   "active"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "social_id"
    t.string   "social_access_token"
    t.string   "profile_image"
    t.string   "social_type"
  end

  add_foreign_key "leader_boards", "users"
  add_foreign_key "master_wallets", "users"
  add_foreign_key "user_coins", "user_transactions"
  add_foreign_key "user_coins", "users"
  add_foreign_key "user_rewards", "rewards"
  add_foreign_key "user_rewards", "users"
  add_foreign_key "user_transactions", "users"
end
