# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Coin.create(name: 'bitcoin', code: 'BTC', image_url: 'https://www.cryptocompare.com/media/19633/btc.png')
Coin.create(name: 'ether', code: 'ETH', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/ethereum.png')
Coin.create(name: 'ripple', code: 'XRP', image_url: 'http://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Ripple-XRP-icon.png')
Coin.create(name: 'bitcoincash', code: 'BCH', image_url: 'https://www.cryptocompare.com/media/1383919/12-bitcoin-cash-square-crop-small-grn.png')
Coin.create(name: 'eos', code: 'EOS', image_url: ' https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/eos.png')
Coin.create(name: 'litecoin', code: 'LTC', image_url: 'https://www.cryptocompare.com/media/19782/litecoin-logo.png')
Coin.create(name: 'stellar lumens', code: 'XLM', image_url: 'https://www.cryptocompare.com/media/20696/str.png')
Coin.create(name: 'cardano', code: 'ADA', image_url: 'http://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/Cardano-ADA-icon.png')
Coin.create(name: 'IOTA', code: 'IOT', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/iota.png')
Coin.create(name: 'tether', code: 'USDT', image_url: 'https://www.cryptocompare.com/media/1383672/usdt.png')
Coin.create(name: 'NEO', code: 'NEO', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/neo.png')
Coin.create(name: 'TRON', code: 'TRX', image_url: 'https://www.cryptocompare.com/media/12318089/trx.png')
Coin.create(name: 'Monero', code: 'XMR', image_url: 'https://www.cryptocompare.com/media/19969/xmr.png')
Coin.create(name: 'Nebulas Token', code: 'NAS', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/nebulas-token.png')
Coin.create(name: 'Ethereum Classic', code: 'ETC', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/ethereum-classic.png')
Coin.create(name: 'NEM', code: 'XEM', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/nem.png')
Coin.create(name: 'Binance coin', code: 'BNB', image_url: 'https://www.cryptocompare.com/media/1383947/bnb.png')
Coin.create(name: 'Wanchain', code: 'WAN', image_url: 'https://storage.googleapis.com/coinmarketpedia/wanchain.png')
Coin.create(name: 'Tezos', code: 'XTZ', image_url: 'https://cdn-images-1.medium.com/max/1200/0*bIvX5KNSYJfxnxUT.png')
Coin.create(name: 'QTUM', code: 'QTUM', image_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Qtum_logo.svg/1135px-Qtum_logo.svg.png')
Coin.create(name: 'ZCash', code: 'ZEC', image_url: ' https://www.cryptocompare.com/media/351360/zec.png')
Coin.create(name: 'OmiseGo', code: 'OMG', image_url: ' https://www.cryptocompare.com/media/1383814/omisego.png')
Coin.create(name: 'Ontology', code: 'ONT', image_url: ' https://coinmarketdaddy.com/upload/coin/11523185662.png')
Coin.create(name: 'ICON Project', code: 'ICX', image_url: 'http://icons.iconarchive.com/icons/cjdowner/cryptocurrency-flat/1024/ICON-ICX-icon.png')
Coin.create(name: 'Zilliqa', code: 'ZIL', image_url: 'https://pbs.twimg.com/media/Dh766mzXkAU9hG9.png')
Coin.create(name: 'List', code: 'LSK', image_url: 'https://cryptoindex.co/coinlogo/lisk.png')
Coin.create(name: 'ByteCoin', code: 'BCN', image_url: ' https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/bytecoin-bcn.png')
Coin.create(name: 'Bitcoin Diamond ', code: 'BCD', image_url: ' https://coinlib.io/static/img/coins/small/xbcd.png,q11345.pagespeed.ic.hIbsWKQQMp.png')
Coin.create(name: 'Decred', code: 'DCR', image_url: ' https://d1ic4altzx8ueg.cloudfront.net/finder-au/wp-uploads/2017/12/decred-logo.png')
Coin.create(name: '0x', code: 'ZRX', image_url: 'https://d1ic4altzx8ueg.cloudfront.net/finder-us/wp-uploads/sites/13/2018/01/0x-coin-250x250.png')
Coin.create(name: 'BitcoinGold', code: 'BTG', image_url: 'https://cdn4.iconfinder.com/data/icons/crypto-currency-and-coin-2/256/bitcoingold_btg_cryptocoin-512.png')
Coin.create(name: 'Eternity', code: 'AE', image_url: ' https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/aeternity.png')
Coin.create(name: 'Bitshares', code: 'BTS', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/bitshares.png')
Coin.create(name: 'Steem', code: 'STEEM', image_url: 'https://www.cryptocompare.com/media/350907/steem.png')
Coin.create(name: 'Verge', code: 'XVG', image_url: 'https://www.cryptocompare.com/media/12318032/xvg.png ')
Coin.create(name: 'Siacoin', code: 'SC', image_url: ' https://www.cryptocompare.com/media/20726/siacon-logo.png')
Coin.create(name: 'Bytom', code: 'BTM', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/bytom.png')
Coin.create(name: 'Augur', code: 'REP', image_url: '  https://www.cryptocompare.com/media/350815/augur-logo.png')
Coin.create(name: 'Maker', code: 'MKR', image_url: ' https://www.cryptocompare.com/media/1382296/mkr.png')
Coin.create(name: 'DigiByte', code: 'DGB', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/digibyte.png')
Coin.create(name: 'Nano', code: 'NANO', image_url: ' https://www.cryptocompare.com/media/30001997/untitled-1.png')
Coin.create(name: 'Dogecoin', code: 'DOGE', image_url: 'https://www.cryptocompare.com/media/19684/doge.png')
Coin.create(name: 'Waves', code: 'WAVES', image_url: 'https://www.cryptocompare.com/media/27010639/waves2.png')
Coin.create(name: 'Golem Network Token', code: 'GNT', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/golem-network-tokens.png')
Coin.create(name: 'Populos', code: 'PPT', image_url: ' https://d1ic4altzx8ueg.cloudfront.net/finder-us/wp-uploads/2018/01/FEATURED.POP_.png')
Coin.create(name: 'Status Network Token', code: 'SNT', image_url: 'https://www.cryptocompare.com/media/1383568/snt.png')
Coin.create(name: 'Chain', code: 'RHOC', image_url: 'https://globalcryptoacademy.com/wp-content/uploads/2018/01/decred-coin-logo.png')
Coin.create(name: 'Basic Attention Token', code: 'BAT', image_url: 'https://raw.githubusercontent.com/condacore/cryptocurrency-icons/master/128x128/basic-attention-token.png')
Coin.create(name: 'Stratis', code: 'STRAT', image_url: ' https://www.cryptocompare.com/media/351303/stratis-logo.png')
Coin.create(name: 'Hshare', code: 'HSR', image_url: ' https://www.cryptocompare.com/media/12318137/hsr.png ')

MinimumTransaction.create(amount: '0.0')

Admin.create(email: 'admin@cryptox.com', password: 'password')
